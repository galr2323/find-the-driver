package com.example.gal.swag;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Created by Gal on 08/04/2015.
 */
public class ParseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ParseObject.registerSubclass(Message.class);
        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "qnZ7o3Q73WXSR0w3A1FA32wNvZTfIVtAXTL7zIkv", "dOT8b2J9JTCXE1zHq7ZaYni7TP9NCjsAGQtPsX51");
    }
}
