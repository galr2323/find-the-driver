package com.example.gal.swag;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Gal on 09/04/2015.
 */
@ParseClassName("Message")
public class Message extends ParseObject {
    public String getUserId() {
        return getString("userId");
    }

    public String getBody() {
        return getString("body");
    }

    public String getDestUserId(){
        return getString("destUserId");
    }

    public void setUserId(String userId) {
        put("userId", userId);
    }

    public void setBody(String body) {
        put("body", body);
    }

    public void setDestUserId(String userId){
        put("destUserId", userId);
    }
}
